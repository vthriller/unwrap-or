#[macro_export]
macro_rules! unwrap_ok_or {
    ($res: expr, $e: pat, $code: expr) => {
        match $res {
            Ok(v) => v,
            Err($e) => $code
        }
    };
}

#[macro_export]
macro_rules! unwrap_err_or {
    ($res: expr, $v: pat, $code: expr) => {
        match $res {
            Ok($v) => $code,
            Err(e) => e
        }
    };
}

#[macro_export]
macro_rules! unwrap_some_or {
    ($res: expr, $code: expr) => {
        match $res {
            Some(v) => v,
            None => $code
        }
    };
}

#[macro_export]
macro_rules! unwrap_none_or {
    ($res: expr, $v: pat, $code: expr) => {
        match $res {
            Some($v) => $code,
            None => ()
        }
    };
}
